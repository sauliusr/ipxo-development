# IPXO development environment


# GIT
```
cd existing_repo
git remote add origin https://gitlab.com/sauliusr/ipxo-development.git
```

## Add a Git Submodule
```
git submodule add <remote_url>
git commit -m "Added the submodule to the project."
git submodule foreach --recursive git checkout development
```

## Pull a Git Submodule
```
git submodule update --init --recursive
```

## Update a Git Submodule
```
git submodule update --remote --merge
```

# Docker
```
docker-compose build
docker-compose up
```
